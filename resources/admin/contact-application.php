<?php

/**
 * contact-application.php - Custom code template contact.
 */

add_action('init', 'custom_page_contact');

function custom_page_contact(){
	// Get the Post ID.
	if(isset($_GET['post']))
		$post_id = $_GET['post'];
	else if(isset($_POST['post_ID']))
		$post_id = $_POST['post_ID'];

	if(!isset($post_id) || empty($post_id))
		return;

	// Get the name of the Page Template file.
	$template_file = get_post_meta($post_id, '_wp_page_template', true);

	// Do something for the template
	if($template_file == "contact"){
		remove_post_type_support('page','comments');
		remove_post_type_support('page','excerpt' );
		remove_post_type_support('page','trackbacks');
		remove_post_type_support('page','revisions');
		remove_post_type_support('page','editor');

		Metabox::make('Formulaire de contact', 'page')->set([
			Field::text('contact_titre1', ['title' => 'Titre']),
			Field::text('contact_form', ['title' => 'Shortcode formulaire de contact']),
			Field::media('contact_image', ['title' => 'Image', 'type' => 'image']),
		]);
	}
}

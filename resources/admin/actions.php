<?php

/**
 * Define WordPress actions for your theme.
 *
 * Based on the WordPress action hooks.
 * https://developer.wordpress.org/reference/hooks/
 *
 */

add_action( 'wp_before_admin_bar_render', 'admin_bar_links' );
function admin_bar_links() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('about');            // Remove the about WordPress link
	$wp_admin_bar->remove_menu('wporg');            // Remove the WordPress.org link
	$wp_admin_bar->remove_menu('documentation');    // Remove the WordPress documentation link
	$wp_admin_bar->remove_menu('support-forums');   // Remove the support forums link
	$wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
	$wp_admin_bar->remove_menu('updates');          // Remove the updates link
	$wp_admin_bar->remove_menu('comments');         // Remove the comments link
	$wp_admin_bar->remove_menu('new-content');      // Remove the content link
	$wp_admin_bar->remove_menu('revisr');      		// Remove revisr
	$wp_admin_bar->remove_menu('view');

}

add_action( 'admin_menu', 'admin_links' );
function admin_links() {
	remove_menu_page('edit.php');					// Remove articles
	remove_menu_page('edit-comments.php');			// Remove commentaires
	remove_menu_page('themes.php');					// Remove apparence
	remove_menu_page('plugins.php');				// Remove plugins
	remove_menu_page('tools.php');					// Remove tools
	remove_menu_page('smush');						// Remove smush
	remove_menu_page('revisr');						// Remove revisr

	add_menu_page(
        '',
        'Menus',
        'manage_options',
        'nav-menus.php',
        '',
        'dashicons-editor-ul',
        21
    );
}

add_filter('admin_footer_text', 'remove_footer_admin');
function remove_footer_admin () {
	echo "<p>Contactez l'agence <a href='https://www.zetruc.fr/' target='_blank'>Zetruc</a> si vous avez besoin d'aide par <a href='mailto:contact@zetruc.fr'>mail</a> ou par <a href='tel:+33325405797'>téléphone</a></p>";
}

function my_login_logo_url() {
	return get_home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
	return get_bloginfo("name");
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

if (get_option('section-slug-coordonnees')) {
	if (array_key_exists('logo', get_option('section-slug-coordonnees'))) {
		if (get_option('section-slug-coordonnees')['logo']) {
			function my_login_logo() { ?>
				<style>
				#login h1 a, .login h1 a {
					background-image: url(<?php echo wp_get_attachment_url(get_option('section-slug-coordonnees')['logo']); ?>);
					height: 120px;
					width: 120px;
					background-size: contain;
				}
				</style>
			<?php }
			add_action( 'login_enqueue_scripts', 'my_login_logo' );
		}
	}
}

<?php

/**
 * about-application.php - Custom code template about.
 */

add_action('init', 'custom_page_about');

function custom_page_about(){
	// Get the Post ID.
	if(isset($_GET['post']))
		$post_id = $_GET['post'];
	else if(isset($_POST['post_ID']))
		$post_id = $_POST['post_ID'];

	if(!isset($post_id) || empty($post_id))
		return;

	// Get the name of the Page Template file.
	$template_file = get_post_meta($post_id, '_wp_page_template', true);

	// Do something for the template
	if($template_file == "about"){
		remove_post_type_support('page','comments');
		remove_post_type_support('page','excerpt' );
		remove_post_type_support('page','trackbacks');
		remove_post_type_support('page','revisions');
		remove_post_type_support('page','editor');

		Metabox::make('Recrutement', 'page')->set([
			Field::text('about_recrutement_titre1', ['title' => 'Titre']),
			Field::editor('about_recrutement_texte', ['title' => 'Texte']),
			Field::media('about_recrutement_image', ['title' => 'Image', 'type' => 'image']),
		]);
		Metabox::make('Equipe', 'page')->set([
			Field::text('about_equipe_titre1', ['title' => 'Titre']),
			Field::text('about_equipe_ceo_titre', ['title' => 'Titre CEO']),
			Field::editor('about_equipe_ceo_texte', ['title' => 'Texte CEO']),
			Field::media('about_equipe_ceo_image', ['title' => 'Image', 'type' => 'image']),
			Field::text('about_equipe_cto_titre', ['title' => 'Titre CTO']),
			Field::editor('about_equipe_cto_texte', ['title' => 'CTO']),
			Field::media('about_equipe_cto_image', ['title' => 'Image', 'type' => 'image']),
		]);
	}
}

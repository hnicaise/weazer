<?php

$sections[] = Section::make('section-slug-coordonnees', 'Infos principales');

$sections[] = Section::make('section-slug-rs', 'Réseaux sociaux');

$sections[] = Section::make('section-slug-appli', 'Application');

$sections[] = Section::make('section-slug-doc', 'Documents');

$page = Page::make('page-options', 'Options')->set();

$page->addSections($sections);

$settings['section-slug-coordonnees'] = array(
	Field::media('logo', ['title' => 'Logo', 'type' => 'image']),
	Field::media('logo_alt', ['title' => 'Logo Footer', 'type' => 'image']),
	Field::text('telephone', ['title' => 'Téléphone']),
	Field::text('mail', ['title' => 'Adresse mail']),
);

$settings['section-slug-rs'] = array(
	Field::text('facebook', ['title' => 'Facebook']),
	Field::text('linkedin', ['title' => 'LinkedIn']),
	Field::text('twitter', ['title' => 'Twitter']),
	Field::text('instagram', ['title' => 'Instagram']),
	Field::text('youtube', ['title' => 'Youtube']),
);

$settings['section-slug-appli'] = array(
	Field::text('url', ['title' => 'Lien']),
	Field::text('apple', ['title' => 'Lien App Store']),
	Field::text('google', ['title' => 'Lien Google Play']),
);

$settings['section-slug-doc'] = array(
	Field::media('cgu', ['title' => 'CGU', 'type' => 'application']),
);

$page->addSettings($settings);

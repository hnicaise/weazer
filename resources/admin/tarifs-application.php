<?php

/**
 * tarifs-application.php - Custom code template tarifs.
 */

add_action('init', 'custom_page_tarifs');

function custom_page_tarifs(){
	// Get the Post ID.
	if(isset($_GET['post']))
		$post_id = $_GET['post'];
	else if(isset($_POST['post_ID']))
		$post_id = $_POST['post_ID'];

	if(!isset($post_id) || empty($post_id))
		return;

	// Get the name of the Page Template file.
	$template_file = get_post_meta($post_id, '_wp_page_template', true);

	// Do something for the template
	if($template_file == "tarifs"){
		remove_post_type_support('page','comments');
		remove_post_type_support('page','excerpt' );
		remove_post_type_support('page','trackbacks');
		remove_post_type_support('page','revisions');
		remove_post_type_support('page','editor');

		Metabox::make('Header', 'page')->set([
			Field::text('header_titre', ['title' => 'Titre']),
		]);

		Metabox::make('Standard', 'page')->set([
			Field::text('standard_titre1', ['title' => 'Titre']),
			Field::editor('standard_texte', ['title' => 'Texte']),
			Field::media('standard_image', ['title' => 'Image', 'type' => 'image']),
		]);
		Metabox::make('Premium', 'page')->set([
			Field::text('premium_titre1', ['title' => 'Titre']),
			Field::editor('premium_texte', ['title' => 'Texte']),
			Field::media('premium_image', ['title' => 'Image', 'type' => 'image']),
		]);
		Metabox::make('Annonce', 'page')->set([
			Field::text('annonce_titre1', ['title' => 'Titre']),
			Field::editor('annonce_texte', ['title' => 'Texte']),
			Field::media('annonce_image', ['title' => 'Image', 'type' => 'image']),
		]);
	}
}

<?php

/**
 * home-application.php - Custom code template home.
 */

add_action('init', 'custom_page_home');

function custom_page_home(){
	// Get the Post ID.
	if(isset($_GET['post']))
		$post_id = $_GET['post'];
	else if(isset($_POST['post_ID']))
		$post_id = $_POST['post_ID'];

	if(!isset($post_id) || empty($post_id))
		return;

	// Get the name of the Page Template file.
	$template_file = get_post_meta($post_id, '_wp_page_template', true);

	// Do something for the template
	if($template_file == "home"){
		remove_post_type_support('page','comments');
		remove_post_type_support('page','excerpt' );
		remove_post_type_support('page','trackbacks');
		remove_post_type_support('page','revisions');
		remove_post_type_support('page','editor');

		Metabox::make('Header', 'page')->set([
			Field::editor('header_texte', ['title' => 'Header']),
			Field::media('header_image', ['title' => 'Image', 'type' => 'image']),
		]);
		Metabox::make('Recrutement', 'page')->set([
			Field::text('recrutement_titre1', ['title' => 'Titre']),
			Field::editor('recrutement_titre2', ['title' => 'Sous titre']),
			Field::editor('recrutement_texte', ['title' => 'Texte']),
			Field::media('recrutement_image', ['title' => 'Image', 'type' => 'image']),
		]);
		Metabox::make('Ils l\'ont adopté', 'page')->set([
			Field::text('adopte_titre1', ['title' => 'Titre']),
			Field::collection('adopte_logo', ['title' => 'Logos', 'type' => 'image']),
		]);
		Metabox::make('Matching', 'page')->set([
			Field::text('matching_titre1', ['title' => 'Titre']),
			Field::editor('matching_titre2', ['title' => 'Sous titre']),
			Field::text('matching_titre_gauche', ['title' => 'Titre colonne gauche']),
			Field::editor('matching_texte_gauche', ['title' => 'Texte colonne gauche']),
			Field::text('matching_titre_droite', ['title' => 'Titre colonne droite']),
			Field::editor('macthing_texte_droite', ['title' => 'Texte colonne droite']),
		]);
		Metabox::make('Personnalite', 'page')->set([
			Field::text('personnalite_titre1', ['title' => 'Titre']),
			Field::editor('personnalite_introduction', ['title' => 'Texte']),
			Field::editor('personnalite_texte', ['title' => 'Texte']),
			Field::media('personnalite_image', ['title' => 'Image', 'type' => 'image']),
		]);
		Metabox::make('Employeur', 'page')->set([
			Field::text('employeur_titre1', ['title' => 'Titre']),
			Field::editor('employeur_texte', ['title' => 'Texte']),
			Field::media('employeur_image', ['title' => 'Image', 'type' => 'image']),
		]);
	}
}

<?php

/**
 * Define your theme custom code.
 */

if(isset($_GET['post']))
	$post_id = $_GET['post'];
else if(isset($_POST['post_ID']))
	$post_id = $_POST['post_ID'];

if(!isset($post_id) || empty($post_id))
	return;

Metabox::make('Meta', 'page')->set([
	Field::text($post_id.'meta_title', ['title' => 'Meta Title']),
	Field::text($post_id.'meta_description', ['title' => 'Meta Description']),
]);

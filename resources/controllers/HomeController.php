<?php

namespace Theme\Controllers;

use Themosis\Route\BaseController;
use Themosis\Metabox\Meta;

class HomeController extends GlobalController
{
    protected $home;

    public function __construct() {
        parent::__construct();

        $get_clients = Meta::get(get_the_id(), 'adopte_logo', true);
        $clients = [];
        foreach ($get_clients as $client) {
            $clients += [
                $client => wp_get_attachment_image($client, 'adopte', ['alt' => get_post_meta($client, '_wp_attachment_image_alt', true)])
            ];
        }

        $this->home = [
            'header' => [
                'texte' => Meta::get(get_the_id(), 'header_texte', true),
                'image' => wp_get_attachment_url(Meta::get(get_the_id(), 'header_image', true))
            ],
            'recrutement' => [
                'titre1' => Meta::get(get_the_id(), 'recrutement_titre1', true),
                'titre2' => Meta::get(get_the_id(), 'recrutement_titre2', true),
                'texte' => Meta::get(get_the_id(), 'recrutement_texte', true),
                'image' => wp_get_attachment_url(Meta::get(get_the_id(), 'recrutement_image', true))
            ],
            'adopte' => [
                'titre1' => Meta::get(get_the_id(), 'adopte_titre1', true),
                'logos' => $clients,
            ],
            'matching' => [
                'titre1' => Meta::get(get_the_id(), 'matching_titre1', true),
                'titre2' => Meta::get(get_the_id(), 'matching_titre2', true),
                'titre-gauche' => Meta::get(get_the_id(), 'matching_titre_gauche', true),
                'texte-gauche' => Meta::get(get_the_id(), 'matching_texte_gauche', true),
                'titre-droite' => Meta::get(get_the_id(), 'matching_titre_droite', true),
                'texte-droite' => Meta::get(get_the_id(), 'macthing_texte_droite', true),
            ],
            'personnalite' => [
                'titre1' => Meta::get(get_the_id(), 'personnalite_titre1', true),
                'intro' => Meta::get(get_the_id(), 'personnalite_introduction', true),
                'texte' => apply_filters('the_content', Meta::get(get_the_id(), 'personnalite_texte', true)),
                'image' => wp_get_attachment_url(Meta::get(get_the_id(), 'personnalite_image', true))
            ],
            'employeur' => [
                'titre1' => Meta::get(get_the_id(), 'employeur_titre1', true),
                'texte' => Meta::get(get_the_id(), 'employeur_texte', true),
                'image' => wp_get_attachment_url(Meta::get(get_the_id(), 'employeur_image', true))
            ],
        ];
        $this->page['meta']['titre'] = $this->site['nom'].' – '.$this->site['description'];

    }

    public function index() {
      return view('home', [
        'home' => $this->home,
        'page' => $this->page,
      ]);
    }
}

<?php

namespace Theme\Controllers;

use Themosis\Route\BaseController;
use Themosis\Metabox\Meta;

class GlobalController extends BaseController
{
    protected $site;
    protected $coordonnees;
    protected $page;
    protected $appli;

    public function __construct() {

        // Informations générales
        $this->site['nom'] = get_bloginfo("name");
        $this->site['description'] = get_bloginfo("description");
        $this->site['url'] = get_home_url();
        $this->site['logo']['principal'] = wp_get_attachment_url(get_option('section-slug-coordonnees')['logo']);
        if (get_option('section-slug-coordonnees')['logo_alt']) {
            $this->site['logo']['alt'] = wp_get_attachment_url(get_option('section-slug-coordonnees')['logo_alt']);
        } else {
            $this->site['logo']['alt'] = $this->site['logo']['principal'];
        }
        $this->site['rs'] = array_filter(get_option('section-slug-rs'));
        $this->site['cgu'] = wp_get_attachment_url(get_option('section-slug-doc')['cgu']);

        // Coordonnées
        $this->coordonnees['telephone']['numero'] = get_option('section-slug-coordonnees')['telephone'];
        $this->coordonnees['telephone']['ugly'] = str_replace(' ', '', $this->coordonnees['telephone']['numero']);;
        $this->coordonnees['mail'] = get_option('section-slug-coordonnees')['mail'];

        // Liens de l'application
        $this->appli = get_option('section-slug-appli');

        // Informations de la page : titre, balises et img couverture
        if (Meta::get( get_the_ID(), get_the_id().'meta_title', true )) {
            $this->page['titre'] = Meta::get( get_the_ID(), get_the_id().'meta_title', true );
        } elseif(get_the_title()) {
            $this->page['titre'] = get_the_title();
        } else {
            $this->page['titre'] =  'Page introuvable – '.$this->site['nom'];
        }

        $this->page['meta']['titre'] = $this->page['titre'].' – '.$this->site['nom'];

        if (Meta::get( get_the_ID(), get_the_id().'meta_description', true )) {
            $this->page['meta']['description'] = Meta::get( get_the_ID(), get_the_id().'meta_description', true );
        } else {
            $this->page['meta']['description'] = $this->site['description'];
        }


        view()->share([
            'site' => $this->site,
            'coordonnees' => $this->coordonnees,
            'page' => $this->page,
            'appli' => $this->appli,
        ]);
    }
}

<?php

namespace Theme\Controllers;

use Themosis\Route\BaseController;
use Themosis\Metabox\Meta;

class TarifsController extends GlobalController
{
    protected $tarifs;

    public function __construct() {
        parent::__construct();

        $this->tarifs = [
            'titre' => Meta::get(get_the_id(), 'header_titre', true),
            'standard' => [
                'titre1' => Meta::get(get_the_id(), 'standard_titre1', true),
                'texte' => apply_filters('the_content', Meta::get(get_the_id(), 'standard_texte', true)),
                'image' => wp_get_attachment_url(Meta::get(get_the_id(), 'standard_image', true)),
            ],
            'premium' => [
                'titre1' => Meta::get(get_the_id(), 'premium_titre1', true),
                'texte' => apply_filters('the_content', Meta::get(get_the_id(), 'premium_texte', true)),
                'image' => wp_get_attachment_url(Meta::get(get_the_id(), 'premium_image', true)),
            ],
            'annonce' => [
                'titre1' => Meta::get(get_the_id(), 'annonce_titre1', true),
                'texte' => apply_filters('the_content', Meta::get(get_the_id(), 'annonce_texte', true)),
                'image' => wp_get_attachment_url(Meta::get(get_the_id(), 'annonce_image', true)),
            ],
        ];
    }

    public function index() {
        return view('tarifs', [
            'tarifs' => $this->tarifs,
        ]);
    }
}

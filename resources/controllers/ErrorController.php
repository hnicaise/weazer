<?php

namespace Theme\Controllers;

use Themosis\Route\BaseController;

class ErrorController extends GlobalController
{

    public function index() {
        return view('error');
    }
}

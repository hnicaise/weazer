<?php

namespace Theme\Controllers;

use Themosis\Route\BaseController;
use Themosis\Metabox\Meta;

class AboutController extends GlobalController
{
    protected $about;

    public function __construct() {
        parent::__construct();

        $this->about = [
            'recrutement' => [
                'titre1' => Meta::get(get_the_id(), 'about_recrutement_titre1', true),
                'texte' => Meta::get(get_the_id(), 'about_recrutement_texte', true),
                'image' => wp_get_attachment_url(Meta::get(get_the_id(), 'about_recrutement_image', true)),
            ],
            'equipe' => [
                'titre1' => Meta::get(get_the_id(), 'about_equipe_titre1', true),
                'ceo' => [
                    'titre' => Meta::get(get_the_id(), 'about_equipe_ceo_titre', true),
                    'texte' => apply_filters('the_content', Meta::get(get_the_id(), 'about_equipe_ceo_texte', true)),
                    'image' => wp_get_attachment_url(Meta::get(get_the_id(), 'about_equipe_ceo_image', true)),
                ],
                'cto' => [
                    'titre' => Meta::get(get_the_id(), 'about_equipe_cto_titre', true),
                    'texte' => apply_filters('the_content', Meta::get(get_the_id(), 'about_equipe_cto_texte', true)),
                    'image' => wp_get_attachment_url(Meta::get(get_the_id(), 'about_equipe_cto_image', true)),
                ]
            ]
        ];
    }

    public function index() {
        return view('about', [
            'about' => $this->about,
        ]);
    }
}

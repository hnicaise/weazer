<?php

namespace Theme\Controllers;

use Themosis\Route\BaseController;
use Themosis\Metabox\Meta;

class GabaritController extends GlobalController
{
    protected $gabarit;

    public function __construct() {
        parent::__construct();

        if (Meta::get( get_the_ID(), 'gabarit_titre', true )) {
            $this->gabarit['titre'] = Meta::get( get_the_ID(), 'gabarit_titre', true );
        } else {
            $this->gabarit['titre'] = get_the_title();
        }

        if (Meta::get( get_the_ID(), 'gabarit_content', true )) {
            $this->gabarit['content'] = apply_filters('the_content', Meta::get( get_the_ID(), 'gabarit_content', true ));
        } else {
            $this->gabarit['content'] = apply_filters('the_content', get_post(get_the_ID())->post_content);
        }
    }

    public function index() {
        return view('gabarit', [
            'gabarit' => $this->gabarit,
        ]);
    }
}

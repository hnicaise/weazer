<?php

namespace Theme\Controllers;

use Themosis\Route\BaseController;
use Themosis\Metabox\Meta;

class ContactController extends GlobalController
{
    protected $contact;

    public function __construct() {
        parent::__construct();

        $this->contact = [
            'titre1' => Meta::get(get_the_ID(), 'contact_titre1', true),
            'form' => do_shortcode(Meta::get(get_the_ID(), 'contact_form', true)),
            'image' => wp_get_attachment_url(Meta::get(get_the_ID(), 'contact_image', true))
        ];
    }

    public function index() {
        return view('contact', [
            'contact' => $this->contact,
        ]);
    }
}

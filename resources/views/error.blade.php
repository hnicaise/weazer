@extends('layout')

@section('content')

    <section class="container">
        <h1>Erreur 404</h1>
        <p>La page que vous recherchez est introuvable</p>
        <a href="{{ $site['url'] }}" class="btn">Retourner à l'accueil</a>
    </section>

@endsection

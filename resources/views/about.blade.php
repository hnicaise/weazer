@extends('layout')

@section('content')
    <section id="about">
        <div class="separator"></div>
        <hr class="hr-pink">
        <section class="bg-pink">
            <div class="nouvelle_maniere row">
                <h1 class="left pink titre-2">{{ $about['recrutement']['titre1'] }}</h1>
                {!! $about['recrutement']['texte'] !!}
                <img class="img-section react" src="{{ $about['recrutement']['image'] }}" alt="{{ $about['recrutement']['titre1'] }}" />
            </div>
        </section>
        <div class="separator"></div>
        <hr class="hr-blue">
        <section class="bg-blue">
            <div class="lequipe row">
                <h2 class="right blue titre-2">{{ $about['equipe']['titre1'] }}</h2>
                <br>
                <div>
                    <div>
                        <img class="img-section react" src="{{ $about['equipe']['ceo']['image'] }}" alt="{{ $about['equipe']['ceo']['titre'] }}" />
                        <h3>{{ $about['equipe']['ceo']['titre'] }}</h3>
                    </div>
                    <div>
                        {!! $about['equipe']['ceo']['texte'] !!}
                    </div>
                </div>
                <div>
                    <div>
                        <img class="img-section react" src="{{ $about['equipe']['cto']['image'] }}" alt="{{ $about['equipe']['cto']['titre'] }}" />
                        <h3><a href="https://mycto.io/" target="_blank">{{ $about['equipe']['cto']['titre'] }}</a></h3>
                    </div>
                    <div>
                        {!! $about['equipe']['cto']['texte'] !!}
                    </div>
                </div>
            </div>
        </section>
        <div class="separator"></div>
    </section>
@endsection

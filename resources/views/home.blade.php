@extends('layout')

@section('content')

    <section id="home">
        <section class="bg-blue">
            <div class="weazer row">
                <h1 class="left">{!! $home['header']['texte'] !!}</h1>
                <div class="">
                    <div class="app">
                        <div class="download">
                            @if ($appli['url'])
                                <a href="{{ $appli['url'] }}" target="_blank">TÉLÉCHARGEZ<br>NOTRE APPLICATION</a>
                            @else
                                <span>TÉLÉCHARGEZ<br>NOTRE APPLICATION</span>
                            @endif
                        </div>
                        <div class="stores">
                            @if ($appli['apple'])
                                <a class="desk" href="{{ $appli['apple'] }}" target="_blank"></a>
                            @else
                                <span class="desk"></span>
                            @endif

                            @if ($appli['google'])
                                <a class="desk" href="{{ $appli['google'] }}" target="_blank"></a>
                            @else
                                <span class="desk"></span>
                            @endif

                            @if ($appli['apple'])
                                <a class="mobile" href="{{ $appli['apple'] }}">
                                    <img src="{{ themosis_theme_assets() }}/images/SVG/apple.svg" alt="App Store" />
                                </a>
                            @else
                                <span class="mobile">
                                    <img src="{{ themosis_theme_assets() }}/images/SVG/apple.svg" alt="App Store" />
                                </span>
                            @endif

                            @if ($appli['google'])
                                <a class="mobile" href="{{ $appli['google'] }}">
                                    <img src="{{ themosis_theme_assets() }}/images/SVG/google.svg" alt="Google Play" />
                                </a>
                            @else
                                <span class="mobile">
                                    <img src="{{ themosis_theme_assets() }}/images/SVG/google.svg" alt="Google Play" />
                                </span>
                            @endif
                        </div>
                    </div>
                    <img class="img-section react" src="{{ $home['header']['image'] }}" alt="{{ $home['header']['texte'] }}" />
                </div>
            </div>
        </section>
        <div class="separator"></div>
        <hr class="hr-pink">
        <section class="bg-pink">
            <div class="recrutez_autrement row">
                <h2 class="right pink titre-2">{{ $home['recrutement']['titre1'] }}</h2>
                <h3 class="j-right titre-3">{!! $home['recrutement']['titre2'] !!}</h3>
                <p class="j-right">{!! $home['recrutement']['texte'] !!}</p>
                <img class="img-section react" src="{{ $home['recrutement']['image'] }}" alt="{{ $home['recrutement']['titre1'] }}" />
            </div>
        </section>
        <div class="separator"></div>
        <hr class="hr-orange">
        <section class="bg-orange">
            <div class="adopte_weazer row">
                <h3 class="center titre-3">{{ $home['adopte']['titre1'] }}</h3>
                <hr class="hr-white">
                <div>
                    @foreach ($home['adopte']['logos'] as $logo)
                        {!! $logo !!}
                    @endforeach
                </div>
            </div>
        </section>
        <div class="separator"></div>
        <section class="bg-blue">
            <div class="matching_innovant row">
                <h2 class="center blue titre-2">{{ $home['matching']['titre1'] }}</h2>
                <h3 class="j-center titre-3">{!! $home['matching']['titre2'] !!}</h3>
                <div>
                    <article>
                        <h4 class="titre-4">{{ $home['matching']['titre-gauche'] }}</h4>
                        <p class="j-left">{!! $home['matching']['texte-gauche'] !!}</p>
                    </article>
                    <img src="{{ themosis_theme_assets() }}/images/right_arrow-min.png" alt="Flèche droite" />
                    <article>
                        <h4 class="titre-4">{{ $home['matching']['titre-droite'] }}</h4>
                        <p class="j-left">{!! $home['matching']['texte-droite'] !!}</p>
                    </article>
                </div>
            </div>
        </section>
        <div class="separator"></div>
        <hr class="hr-orange">
        <section class="bg-orange">
            <div class="recrutez_personnalite row">
                <h2 class="left orange titre-2">{{ $home['personnalite']['titre1'] }}</h2>
                <p class="left">{!! $home['personnalite']['intro'] !!}</p>
                <div>
                    {!! $home['personnalite']['texte'] !!}
                </div>
                <img class="img-section react" src="{{ $home['personnalite']['image'] }}" alt="{{ $home['recrutement']['titre1'] }}" />
            </div>
        </section>
        <div class="separator"></div>
        <div class="separator separator2"></div>
        <hr class="hr-pink">
        <section class="bg-pink">
            <div class="marque_employeur row">
                <img class="img-section react" src="{{ $home['employeur']['image'] }}" alt="{{ $home['employeur']['titre1'] }}" />
                <h2 class="right pink titre-2">{{ $home['employeur']['titre1'] }}</h2>
                <p class="left">{!! $home['employeur']['texte'] !!}</p>
            </div>
        </section>
        <div class="separator"></div>
    </section>

@endsection

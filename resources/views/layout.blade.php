<!DOCTYPE html>
<html lang="fr-FR">
	<head>
		<!-- Basic stuff =========== -->
		<meta charset="utf-8">
		<title>{{ $page['meta']['titre'] }}</title>
		<meta name="description" content="{{ $page['meta']['description'] }}">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="format-detection" content="telephone=no">
		<link rel="apple-touch-icon" sizes="180x180" href="{{ themosis_assets() }}/images/logo/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="{{ themosis_assets() }}/images/logo/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="{{ themosis_assets() }}/images/logo/favicon-16x16.png">
		<link rel="manifest" href="{{ themosis_assets() }}/images/logo/site.webmanifest">
		<link rel="mask-icon" href="{{ themosis_assets() }}/images/logo/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="theme-color" content="#5bbad5">

		<!-- Import Styles -->
		<link rel="stylesheet" href="{{ themosis_assets() }}/css/custom.min.css">
		@yield('styles')

		<!-- Import JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		@yield('javascriptheader')

		<!-- Import Cookie -->
		<link href="https://fonts.googleapis.com/css?family=Comfortaa%7CMontserrat:400,700&display=swap" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
		<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
		<script>
		window.addEventListener("load", function(){
		window.cookieconsent.initialise({
		  "palette": {
		    "popup": {
		      "background": "#06b4e1",
		      "text": "#fff"
		    },
		    "button": {
		      "background": "#eeae40",
		      "text": "#fff"
		    }
		  },
		  "type": "opt-in",
		  "content": {
		    "message": "En poursuivant votre navigation sur notre site, vous acceptez l'utilisation de Cookies. Les Cookies assurent le bon fonctionnement de nos services notamment pour réaliser des statistiques de visites anonymes. ",
		    "deny": "Je refuse",
		    "allow": "J'accepte",
		    "link": "En savoir plus",
		    "href": "http://www.google.com/analytics/terms/fr.html"
		  }
		})});
		</script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143197201-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-143197201-1');
		</script>
	</head>
	<body>
		<div id="wrapper" class="{{ get_post_type(Loop::id()) }} {{ get_post_meta(Loop::id(), '_wp_page_template', true) }} @if(is_404()) error @endif">
			@include('header')

			<main>
				@yield('content')
			</main>

			@include('footer')
		</div>

		<!-- Import JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
		<script src="{{ themosis_assets() }}/js/custom.min.js"></script>
		{{-- <script id="__bs_script__">
			//<![CDATA[
		   	document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.3'><\/script>".replace("HOST", location.hostname));
			//]]>
		</script> --}}
		@yield('javascript')
	</body>
</html>

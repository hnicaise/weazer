@extends('layout')

@section('content')

    @if ($gabarit['content'])
        <section class="container">
            <h1>{{ $gabarit['titre'] }}</h1>
            {!! $gabarit['content'] !!}
        </section>
    @else
        <section class="empty">
            <div class="separator"></div>
            <hr class="hr-orange">
            <section class="bg-orange">
                <div class="recrutez_personnalite row">
                    <h1 class="left orange titre-2">Gardez l'oeil ouvert !</h1>
                    <p class="left">Les informations arrivent prochainement sur votre site {{ $site['nom'] }}</p>
                    <img class="img-section react" src="{{ themosis_assets() }}/images/react2-min.png" alt="Gardez l'oeil ouvert !" />
                </div>
            </section>
        </section>
    @endif

@endsection

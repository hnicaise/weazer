<footer>
    <div class="container">
        <div>
            <a href="{{ $site['url'] }}">
                <img class="logo_w" src="{{ $site['logo']['alt'] }}" alt="Logo {{ $site['nom']}}" />
            </a>
            <a href="https://lafrenchtech.com/fr/" target="_blank" rel="nofollow">
                <img src="{{ themosis_assets() }}/images/french-tech-min.png" alt="La French Tech" style="filter: brightness(0) invert(1);">
            </a>
            <div class="coords">
                <a href="mailto:{{ $coordonnees['mail'] }}">{{ $coordonnees['mail'] }}</a><br/>
                <a href="tel:{{ $coordonnees['telephone']['ugly'] }}">{{ $coordonnees['telephone']['numero'] }}</a>
            </div>
            <div class="button">
                <a href="#home">Téléchargez<br>notre application</a>
            </div>
            <div class="social">
                <div>
                    @foreach ($site['rs'] as $rs => $lien)
                        <a href="{{ $lien }}" target="_blank">
                            <i class="font-icon-{{ $rs }}"></i>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <p id="copyright">
        © 2019 – {{ $site['nom'] }} par <a href="https://www.zetruc.fr/" rel="nofollow" title="Agence de Communication à Troyes et à Reims" target="_blank">Zetruc</a>. Tous droits réservés.
        <a href="{{ $site['url'] }}/mentions-legales">Mentions légales</a> - <a href="{{ $site['url'] }}/politique-confidentialite">Politique de confidentialité</a> - <a href="{{ $site['cgu'] }}" target="_blank">CGU</a>
    </p>
</footer>
<script>
$(document).ready(function() {
    $('.pop').click((function(toggleValue){
        return function(e){
            if(toggleValue){//first action
            $("nav").css("top","0px");
            $('.pop').css("fill","white");
            }else{//second action
            $("nav").css("top","");
            $('.pop').css("fill","");
            }
            toggleValue = !toggleValue;//toggle
        }
    })(true)/** IIFE */);
});
</script>

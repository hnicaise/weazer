@section('styles')
    <link rel="stylesheet" href="{{ $site['url'] }}/content/plugins/contact-form-7/includes/css/styles.css" property="stylesheet">
@endsection

@extends('layout')

@section('content')

    <section id="contact">
        <div class="separator"></div>
        <hr class="hr-orange">
        <section class="bg-orange">
            <div class="formulaire row">
            <h1 class="left orange titre-2">{!! $contact['titre1'] !!}</h1>
            {!! $contact['form'] !!}
            <img class="img-section" src="{{ $contact['image'] }}" alt="{{ $contact['titre1'] }}" />
            </div>
        </section>
        <div class="separator"></div>
    </section>

@endsection


@section('javascript')

<script>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"{{ $site['url'] }}/wp-json/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Merci de confirmer que vous n\u2019\u00eates pas un robot."}}};
    /* ]]> */
</script>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script src='{{ plugins_url() }}/contact-form-7/includes/js/scripts.js'></script>

@endsection

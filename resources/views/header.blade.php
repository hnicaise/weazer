<header id="page-header">
    <a href="{{ $site['url'] }}" class="logo"><img  src="{{ $site['logo']['principal'] }}" alt="Logo {{ $site['nom'] }}" /></a>
        <svg class="pop" height="32px" id="Layer_1" style="enable-background:new 0 0 32 32;" version="1.1" viewBox="0 0 32 32" width="32px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z"/></svg>
        <nav class="mob_nav">{!! wp_nav_menu(['theme_location' => 'header-nav']) !!}</nav>
        <nav class="desk_nav">
            <hr class="hr-blue">
            <div class="container">
                <div class="social">
                    @foreach ($site['rs'] as $rs => $lien)
                        <a href="{{ $lien }}" target="_blank">
                            <i class="font-icon-{{ $rs }}"></i>
                        </a>
                    @endforeach
                </div>
                {!! wp_nav_menu(['theme_location' => 'header-nav']) !!}
            </div>
            <hr class="hr-blue">
        </nav>
</header>

@extends('layout')

@section('content')
    <section id="tarifs">
        <div class="container">
            <h1>{{ $tarifs['titre'] }}</h1>
        </div>
        <hr class="hr-blue">
        <section class="bg-blue">
            <div class="standard row">
                <h2 class="left blue titre-2">{{ $tarifs['standard']['titre1'] }}</h2>
                {!! $tarifs['standard']['texte'] !!}
                <a href="mailto:{{ $coordonnees['mail'] }}" class="sub orange">SOUSCRIRE</a>
                <img class="img-section react right" src="{{ $tarifs['standard']['image'] }}" alt="{{ $tarifs['standard']['titre1'] }}" />
            </div>
        </section>
        <div class="separator"></div>
        <hr class="hr-pink">
        <section class="bg-pink">
            <div class="premium row">
                <h2 class="right pink titre-2">{{ $tarifs['premium']['titre1'] }}</h2>
                <img class="img-section react left" src="{{ $tarifs['premium']['image'] }}" alt="{{ $tarifs['premium']['titre1'] }}" />
                {!! $tarifs['premium']['texte'] !!}
                <a href="mailto:{{ $coordonnees['mail'] }}" class="sub blue">SOUSCRIRE</a>
            </div>
        </section>
        <div class="separator"></div>
        <hr class="hr-orange">
        <section class="bg-orange">
            <div class="standard row">
                <h2 class="left orange titre-2">{{ $tarifs['annonce']['titre1'] }}</h2>
                {!! $tarifs['annonce']['texte'] !!}
                <a href="mailto:{{ $coordonnees['mail'] }}" class="sub pink">SOUSCRIRE</a>
                <img class="img-section react right" src="{{ $tarifs['annonce']['image'] }}" alt="{{ $tarifs['annonce']['titre1'] }}" />
            </div>
        </section>
        <div class="separator"></div>
    </section>
@endsection

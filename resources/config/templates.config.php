<?php

return [

    /**
     * Edit this file in order to configure your theme templates.
     *
     * Simply define a template slug.
     *
     * Since WordPress 4.7, templates can be defined for pages but
     * also on custom post types.
     */
    'home' => ['Accueil', ['page']],
	'tarifs' => ['Tarifs', ['page']],
	'about' => ['À propos', ['page']],
    'contact' => ['Contact', ['page']],
];

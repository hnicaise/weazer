<?php

/**
 * Define your routes and which views to display
 * depending of the query.
 *
 * Based on WordPress conditional tags from the WordPress Codex
 * http://codex.wordpress.org/Conditional_Tags
 *
 */

Route::get('template', ['home', 'uses' => 'HomeController@index']);
Route::get('template', ['contact', 'uses' => 'ContactController@index']);
Route::get('template', ['gabarit', 'uses' => 'GabaritController@index']);
Route::get('template', ['tarifs', 'uses' => 'TarifsController@index']);
Route::get('template', ['about', 'uses' => 'AboutController@index']);

Route::get('404', ['404', 'uses' => 'ErrorController@index']);
Route::get('page', ['uses' => 'GabaritController@index']);

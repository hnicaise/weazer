var webpack = require('webpack'),
    path = require('path');

module.exports = {
    cache: true,
    target: 'web',
    entry: {
        theme: path.join(__dirname, 'assets/js/custom.js'),
    },
    output: {
        path: path.join(__dirname, 'dist/js'),
        publicPath: '',
        filename: 'custom.min.js'
    },
    module: {},
    plugins: []
};

$(document).ready(function() {

    /* Scripts
    ================================================== */
    if ($('#wrapper.about').length > 0 ) {
        $('.lequipe > img:first-of-type').prependTo('.lequipe div:first-of-type');
        $('.lequipe > img:last-of-type').appendTo('.lequipe div:last-of-type');
    }
});
